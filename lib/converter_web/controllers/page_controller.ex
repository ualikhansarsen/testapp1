defmodule ConverterWeb.PageController do
  use ConverterWeb, :controller

  def index(conn, _params) do
    valutes = Enum.sort(Map.keys(Converter.ConverterServer.get_value()))
    render(conn, "index.html", %{conn: conn, result: "", valutes: valutes})
  end

  def convert(conn, params) do

    curr1 = params["currency1"]
    curr2 = params["currency2"]
    {value, _} = Float.parse(params["value"])
    currs = Converter.ConverterServer.get_value()

    result = Converter.Convertion.convert(currs, curr1, curr2, value)

    valutes = Enum.sort(Map.keys(currs))

    render(conn, "index.html", %{conn: conn, result: result, valutes: valutes})

  end

  def get_api(conn, params) do

    try do
      curr1 = params["currency1"]
      curr2 = params["currency2"]
      {value, _} = Float.parse(params["value"])
      currs = Converter.ConverterServer.get_value()

      result = Converter.Convertion.convert(currs, curr1, curr2, value)

      render(conn, "answer.html", %{result: result})

    rescue
      ErlangError -> render(conn, "answer.html", %{result:  "None"})
    end

  end
end
