defmodule Converter.Crawler do
    use HTTPoison.Base
    def process_response_body(body) do
        body = Jason.decode!(body)
        Enum.map(body["Valute"], fn({k, v}) -> {k, v["Value"] / v["Nominal"]} end) # elem(Float.parse(v["Value"]), 0)} end)
    end
end
