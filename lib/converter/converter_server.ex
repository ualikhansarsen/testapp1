defmodule Converter.ConverterServer do
    use GenServer
    alias Converter.Crawler

    def init(filename) do
        :dets.open_file(filename, [{:type, :set}])
        __MODULE__.load()
        {:ok, filename}
    end

    def handle_info(:load, state) do
        Crawler.start
        Enum.map(Crawler.get!("https://www.cbr-xml-daily.ru/daily_json.js").body, fn(pair) -> :dets.insert(state, pair) end)
        schedule_work()
        {:noreply, state}
    end

    def handle_call(:get_value, _from, state) do
        data = :dets.match(state, {:"$1", :"$2"})
        data =  Enum.into(data, %{}, fn([a, b]) -> {a, b} end)
        {:reply, data, state}
    end

    defp schedule_work() do
        Process.send_after(self(), :load, 24 * 60 * 60 * 1000)
    end

    ## Client API
    def start_link(ival) do
        GenServer.start_link(__MODULE__, ival, [{:name, __MODULE__}])
    end

    def load, do: send( __MODULE__, :load)
    def get_value, do: GenServer.call(__MODULE__, :get_value)
end
