defmodule Converter.Crawler.CrawlerSpec do
  use ESpec

  alias Converter.Crawler
  describe "crawler spec" do
    context "Load data from json" do
      it "test" do 
        dtt = Crawler.get!("https://www.cbr-xml-daily.ru/daily_json.js").body
        List.first(dtt) |> should(eq {"HKD", 83.1268})
        List.last(dtt) |> should(eq {"AUD", 45.5022})
      end
    end
  end
end