defmodule Converter.ConverterServerSpec do

    use ESpec

    describe "converter server spec" do

        before do

            Converter.Crawler.start
            llist = Converter.Crawler.get!("https://www.cbr-xml-daily.ru/daily_json.js").body
            # llist stands for list of lists

            {:ok, pid} = Converter.ConverterServer.start_link(:test_file)
            # allow Converter.ConverterServer |> to(accept(:get_list, fn() -> [["CNZ", 94.2565], ["AUD", 45.5022]] end))

            llist = Enum.sort(
                Enum.map(llist,
                fn({a, b}) -> [a, b] end),
            fn([a, _], [b, _]) -> a < b end
            )

            {:shared, pid: pid, llist: llist}

        end

        finally do: GenServer.stop(shared.pid)

        #after_all do
        #    System.cmd("rm", ["test_file"])
        #end

        it "ConverterServer should write data to dets" do
            Converter.ConverterServer.load()
            data = :dets.match(:test_file, {:"$1", :"$2"})

            data = Enum.sort(data, fn([a, _], [b, _]) -> a < b end)

            IO.inspect data
            IO.inspect shared.llist

            List.first(data) |> should(eq List.first(shared.llist))
            List.last(data) |> should(eq List.last(shared.llist))
        end

        it "ConverterServer should return the writen data" do
            data = Converter.ConverterServer.get_value()

            llist =  Enum.into(shared.llist, %{}, fn([a, b]) -> {a, b} end)

            data["AUD"] |> should(eq llist["AUD"])
            true |> should(eq true)
        end
    end
end
